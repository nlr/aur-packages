stages:
  - package
  - test
  - deploy

variables:
  GIT_USER_NAME: "[BOT] Gilbert Gilb's"
  GIT_USER_EMAIL: "gilbsgilbert@gmail.com"

render:
  stage: package
  image: python:3.12-slim
  script:
    - pip install -r requirements.txt
    - ./render.py "${PACKAGE}"
    # Remove other projects to prevent their metadata from appearing in the
    # artifacts.
    - find ./pkg/ -mindepth 1 -maxdepth 1 -type d ! -name "${PACKAGE}" -exec rm -Rf {} +
  artifacts:
    paths:
      - ./dist/
      - ./pkg/*/_metadata.json
    expire_in: 1 day

build-package:
  stage: package
  needs: [render]
  image: archlinux:latest
  script:
    # Install dev dependencies
    - pacman -Syu --noconfirm base-devel sudo

    # Create build user and configure makepkg
    - useradd --create-home builder
    - echo 'builder ALL=(ALL) ALL' >> /etc/sudoers.d/builder
    - echo 'Defaults:builder !authenticate' >> /etc/sudoers.d/builder
    - PKGDEST="$(pwd)/dist/_packages/"
    - SRCDEST=/home/builder/srcdest/
    - mkdir -p "${PKGDEST}" "${SRCDEST}"
    - chown builder:builder "${PKGDEST}" "${SRCDEST}"
    - mkdir -p /etc/makepkg.conf.d/custom.conf.d
    - echo "SRCDEST='${SRCDEST}'" >> /etc/makepkg.conf.d/custom.conf
    - echo "PKGDEST='${PKGDEST}'" >> /etc/makepkg.conf.d/custom.conf
    - echo "COMPRESSZST=(zstd -c -T0 --fast=3 -)" >> /etc/makepkg.conf.d/custom.conf

    # Build package
    - cd "dist/${PACKAGE}"
    - sudo -Hu builder makepkg --printsrcinfo > .SRCINFO
    - sudo -Hu builder makepkg --noconfirm --syncdeps --rmdeps

    # Sync package (can't use an artifact because it may be >1GB)
    - pacman -S --noconfirm rclone
    - |
      rclone copy \
        --config "${RCLONE_CONFIG_FILE}" \
        --progress \
        --filter "+ /${PACKAGE}-*.pkg.tar.zst" \
        --filter '- *' \
        "${CI_PROJECT_DIR}/dist/_packages/" \
        "aur-packages:${CI_PIPELINE_ID}"
  artifacts:
    paths:
      - ./dist/*/.SRCINFO
    expire_in: 1 day

test-package:
  stage: test
  image: archlinux:latest
  before_script:
    - pacman -Syu --noconfirm

    # Download package
    - pacman -S --noconfirm rclone
    - |
      rclone copy \
        --config "${RCLONE_CONFIG_FILE}" \
        --progress \
        --filter "+ /${PACKAGE}-*.pkg.tar.zst" \
        --filter '- *' \
        "aur-packages:${CI_PIPELINE_ID}" \
        "${CI_PROJECT_DIR}/dist/_packages/"

    # Install package
    - pacman -U --noconfirm dist/_packages/${PACKAGE}-*.pkg.tar.zst
  script:
    - echo "override this"
    - exit 1

push-package:
  stage: deploy
  resource_group: push-package
  image: python:3.10-slim
  script:
    - apt-get update && apt-get install -y git openssh-client
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com aur.archlinux.org >> ~/.ssh/known_hosts
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - git config --global user.name "${GIT_USER_NAME}"
    - git config --global user.email "${GIT_USER_EMAIL}"
    - pip install -r requirements.txt
    - ./push.py "${PACKAGE}"
